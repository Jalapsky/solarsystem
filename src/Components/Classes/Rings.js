import * as THREE from "three";


export const Rings = (innerRadius, outerRadius, Segments, textureRings) => {
    const texture = new THREE.TextureLoader().load(`src/Assets/textures/${textureRings}.png`);


    const rings = new THREE.Mesh(
        new THREE.RingGeometry(innerRadius, outerRadius, Segments),
        new THREE.MeshStandardMaterial({ color: 0xffffff, side: THREE.DoubleSide, map: texture, transparent: true })
    );
    return  rings
}


// const geometry = new THREE.RingGeometry( 1, 5, 32 );
// const material = new THREE.MeshBasicMaterial( { color: 0xffff00, side: THREE.DoubleSide } );
// const mesh = new THREE.Mesh( geometry, material );
