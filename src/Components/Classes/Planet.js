import * as THREE from "three";

export const Planet = (radius, width, height, texturePlanet) => {
    const texture = new THREE.TextureLoader().load(`src/Assets/textures/${texturePlanet}.jpg`);
    const planet = new THREE.Mesh(
        new THREE.SphereGeometry(radius, width, height),
        new THREE.MeshStandardMaterial({
            map: texture,
        })
    );

    return planet;
}
