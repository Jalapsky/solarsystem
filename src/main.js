import './Assets/css/style.css'
import * as THREE from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {Planet} from "./Components/Classes/Planet";
import {Rings} from "./Components/Classes/Rings";

let time = 0;



const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 50000);
const scene = new THREE.Scene();
const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#bg')
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
camera.position.setZ(100);
camera.position.setX(-80)
camera.position.setY(100)
renderer.render(scene, camera)




const pointLight = new THREE.PointLight(0x95a5a6)
pointLight.position.set(1,1,1)
const ambientLight = new THREE.AmbientLight(0xffffff)
scene.add(pointLight, ambientLight)


const lightHelper = new THREE.PointLight(pointLight)
const gridHelper = new THREE.GridHelper(200, 50)
scene.add(lightHelper, )




const controls = new OrbitControls(camera, renderer.domElement);

function addStar() {
    const geometry = new THREE.SphereGeometry(0.25, 24, 24);
    const material = new THREE.MeshStandardMaterial({color: 0xffffff});
    const star = new THREE.Mesh(geometry, material);

    const [x,y,z] = Array(3).fill().map(() => THREE.MathUtils.randFloatSpread(600));

    star.position.set(x, y, z);
    scene.add(star);
}

Array(600).fill().forEach(addStar);



const spaceTexture = new THREE.TextureLoader().load('src/Assets/textures/space.jpg');
scene.background = spaceTexture;


//Planets

//Sun
const sun = Planet(40, 32, 32, 'sun');
scene.add(sun);

//Mercury
const mercury = Planet(1, 32, 32, 'mercury');
scene.add(mercury)

//Venus
const venus = Planet(3, 32, 32, 'venus');
scene.add(venus)
//Earth with Moon
const earth = Planet(5, 32, 32, 'earth')
scene.add(earth);

const moon = Planet(1, 32, 32, 'moon')
scene.add(moon)


//Mars
const mars = Planet(3, 32, 32, 'mars');
scene.add(mars);

//Jupiter
const jupiter = Planet(10, 32, 32, 'jupiter');
scene.add(jupiter);

//Saturn
const saturn = Planet(10, 32, 32, 'saturn');
scene.add(saturn);


const saturnRings = Rings(13, 20, 50, 'saturnRings');
scene.add(saturnRings);


//Uranus
const uranus = Planet(6, 32, 32, 'uranus');
scene.add(uranus);





setInterval(() => {
    time += Math.PI/5000;

    mercury.position.x = Math.sin(time*12) * 50;
    mercury.position.z = Math.cos(time*12) * 50;

    venus.position.x = Math.sin(time*10) * 70;
    venus.position.z = Math.cos(time*10) * 70;


    earth.position.x = Math.sin(time*8) * 90;
    earth.position.z = Math.cos(time*8) * 90;

    moon.position.x = earth.position.x + Math.cos(time*50) * 8;
    moon.position.z = earth.position.z + Math.sin(time*50) * 8;
    moon.position.y = earth.position.y * 2;


    mars.position.x = Math.sin(time*6) * 110;
    mars.position.z = Math.cos(time*6) * 110;

    jupiter.position.x = Math.sin(time*4) * 140;
    jupiter.position.z = Math.cos(time*4) * 140;

    saturn.position.x = Math.sin(time*2) * 160;
    saturn.position.z = Math.cos(time*2) * 160;

    saturnRings.position.x = saturn.position.x + Math.sin(time);
    saturnRings.position.z = saturn.position.z + Math.cos(time);
    saturnRings.position.y = saturn.position.y;


    uranus.position.x = Math.sin(time*2.5) * 185;
    uranus.position.z = Math.cos(time*2.5) * 185;


}, 10)



function animate() {
    requestAnimationFrame( animate );

    sun.rotation.y += 0.0005;
    mercury.rotation.y += 0.1;
    venus.rotation.y += 0.090;
    earth.rotation.y+= 0.080;
    moon.rotation.x += 0.090;
    mars.rotation.x += 0.080;
    jupiter.rotation.y += 0.040;
    saturn.rotation.y += 0.040;
    saturnRings.rotation.x = 90;
    uranus.rotation.y += 0.050;




    controls.update();

    renderer.render(scene, camera);
}


animate();
