/*
Описание и экспорт функций Actions
*/

import {
    ALL_USERS, LOG_OUT,
    LOGGED_IN, ROLES_CARD_CONFIG,
    NAKS_DATA, NAKS_RESULTS,
    KSS_DATA, KSS_RESULTS,
    STAFF_DATA, STAFF_RESULTS,
    ALL_CHATS,
    SET_CHATSOCKET,
} from "./types";

export function updateState(data) {
    return{
        type: LOGGED_IN,
        payload: data
    }
}

export function getRolesConfig(data) {
    return{
        type: ROLES_CARD_CONFIG,
        payload: data,
    }
}

export function logOut() {
    return{
        type: LOG_OUT
    }
}


// Naks

export function naksData(data) {
    return{
        type: NAKS_DATA,
        payload: data,
    }
}

export function naksResults(data) {
    return{
        type: NAKS_RESULTS,
        payload: data,
    }
}

// Kss

export function kssData(data) {
    return{
        type: KSS_DATA,
        payload: data,
    }
}

export function kssResults(data) {
    return{
        type: KSS_RESULTS,
        payload: data,
    }
}

//Staff

export function staffData(data) {
    return{
        type: STAFF_DATA,
        payload: data,
    }
}

export function staffResults(data) {
    return{
        type: STAFF_RESULTS,
        payload: data,
    }
}


export function setChatSocket(data) {
    return{
        type: SET_CHATSOCKET,
        payload: data,
    }
}
