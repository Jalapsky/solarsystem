/*
Инициализация и экспорт компонента store
*/

import {createStore} from "redux";
import rootReducer from "./rootReducer";
import { compose } from "redux";


const store = createStore(
    rootReducer,
    compose(
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    )
)

export default store;