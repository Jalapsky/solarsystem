/*
Преобразование функций, отвечающих за разные части
состояния, в общую функцию.
*/

import {combineReducers} from "redux";
import {mainReducer} from "./reducers/MainReducer";

const rootReducer = combineReducers({
    mainReducer: mainReducer,
})

export default rootReducer;
