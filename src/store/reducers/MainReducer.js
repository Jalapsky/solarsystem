/*
Инициализация функций, отвечающих за запись данных
об авторизации. Формирование редьюсера для этих
данных.
*/

const CREATE_SCENE = 'CREATE_SCENE';
const AORPI_RESULTS = 'AORPI_RESULTS';
const LOAD_AORPI = 'LOAD_AORPI'

export function createScene(data) {
    return{
        type: CREATE_SCENE,
        payload: data
    }
}

export function aorpiResults(data) {
    return{
        type: AORPI_RESULTS,
        payload: data
    }
}

export function loadAorpi(data) {
    return{
        type: LOAD_AORPI,
        payload: data
    }
}

const initialAorpi = {
    scene: null,
    camera: null,
    renderer: null,
    time: 0,
    results: null,
    loading: false
}

export function mainReducer(state = initialAorpi, action) {
    switch (action.type) {
        case CREATE_SCENE:
            return {...state, scene: action.payload}
        case AORPI_RESULTS:
            return {...state, results: action.payload}
        case LOAD_AORPI:
            return {...state, loading: action.payload}
        default: return state
    }
}
